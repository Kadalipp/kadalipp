## Kommentaarid töö lahenduse kohta

#### Töö protsess
3-5 päeva materjali lugemist

1 päev algne lahenduse, mida probleemide tekkimise tõttu kasutada ei saanud

2-3 päeva materjali lugemist

30\. märts - 1. mai hetkene lahendus nullist

## Built With

- ReactJS
- React Hooks

## How To Start

### One Click Download & Run
You can download and run the repo with one command

`git clone https://bitbucket.org/Kadalipp/kadalipp.git && cd kadalipp && npm install && npm start`

### Setup

Clone the repository and change directory:

```
git clone https://bitbucket.org/Kadalipp/kadalipp.git
cd kadalipp
```

Install npm dependencies:

```
npm install
```

Run the app locally:

```
npm start
```