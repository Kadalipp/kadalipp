import React, {useState} from 'react';
import Game from './Game';
import Sidebar from './Sidebar';


const App = () => {
  const [numberOfElements, setNumberOfElements] = useState(10)

  function onNumberOfElementsChange(value){
    setNumberOfElements(parseInt(value))
  }

  return (
    <div className="app">
      <Sidebar
        numberOfElements={numberOfElements}
        updateNumberOfElements={onNumberOfElementsChange}
      />
      <Game numberOfElements={numberOfElements}/>
    </div>
  )
}

export default App;