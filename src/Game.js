import React, {useEffect, useRef, useState} from 'react';
import './App.css';


function draw(ctx, location) {
  ctx.fillStyle = location.color;
  ctx.fillRect(location.x, location.y, 20, 20);
}

function createDistanceMatrix(locations) {
  const matrix = Array.from(Array(locations.length), () => Array(locations.length))
  let distance
  for (let row = 0; row < locations.length; row++) {
    for (let col = row + 1; col < locations.length; col++) {
      distance = Math.sqrt((locations[row].x - locations[col].x) ** 2 + (locations[row].y - locations[col].y) ** 2)
      matrix[row][col] = distance
      matrix[col][row] = distance
    }
  }
  return matrix
}


function randomDirection(speedMin = 0, speedMax = 4) {
  return Math.floor(Math.random() * (speedMax - speedMin) + speedMin + 1) * (Math.random() >= 0.5 ? 1 : -1)
}


function createElements(numberOfElements, canvasRef) {
  return Array.from(Array(numberOfElements),
    () => ({
      x: Math.floor(Math.random() * (canvasRef.current.width - 20)) + 10,
      y: Math.floor(Math.random() * (canvasRef.current.height - 20)) + 10,
      vx: randomDirection(),
      vy: randomDirection(),
      color: "#000000"
    }));
}


function updateLocation(loc, mouseCoords, distanceFromOthers, canvasRef, safeDistance = 50) {
  const bounds = canvasRef.current.getBoundingClientRect();
  const distanceFromMouse = Math.sqrt((loc.x - mouseCoords.x + bounds.left) ** 2 + (loc.y - mouseCoords.y + bounds.top) ** 2)

  if (distanceFromMouse < safeDistance * 2) {
    do {
      loc.vy = randomDirection(7, 9)
      loc.vx = randomDirection(7, 9)
    } while (Math.sqrt((loc.x - mouseCoords.x + bounds.left + loc.vx) ** 2 + (loc.y - mouseCoords.y + bounds.top + loc.vy) ** 2) < distanceFromMouse)
    loc.color = "#00ff00"
  } else if (distanceFromOthers.some(distance => distance < safeDistance)) {
    loc.vy = randomDirection(3, 7)
    loc.vx = randomDirection(3, 7)
    loc.color = "#ff0000"
  } else if (loc.color === "#ff0000" || loc.color === "#00ff00") {
    loc.vy = Math.floor(loc.vy / 1.5)
    loc.vx = Math.floor(loc.vx / 1.5)
    loc.color = "#000000"
  }

  if (loc.y + loc.vy > canvasRef.current.height - 10 || loc.y + loc.vy < 0) {
    loc.vy = -loc.vy;
  }
  if (loc.x + loc.vx > canvasRef.current.width - 10 || loc.x + loc.vx < 0) {
    loc.vx = -loc.vx;
  }

  loc.x += loc.vx;
  loc.y += loc.vy;
  return loc;
}


function useCanvas(initialElements = []) {
  const [locations, setLocations] = useState(initialElements)
  const canvasRef = useRef(null)

  useEffect(() => {
    const canvas = canvasRef.current;
    const ctx = canvas.getContext('2d')
    const bounds = canvas.getBoundingClientRect();
    const canvasWidth = window.innerWidth - bounds.left
    const canvasHeight = window.innerHeight - bounds.top

    ctx.clearRect(0, 0, canvasWidth, canvasHeight)
    ctx.canvas.width = canvasWidth;
    ctx.canvas.height = canvasHeight;
    locations.forEach(location => draw(ctx, location))
  })
  return [locations, setLocations, canvasRef]
}


const Canvas = ({mouseCoordinates, numberOfElements}) => {
  const [locations, setLocations, canvasRef] = useCanvas()

  useEffect(() => {
    function update() {
      const distanceMatrix = createDistanceMatrix(locations)
      setLocations(locations.map((loc, index) =>
        updateLocation(loc, mouseCoordinates, distanceMatrix[index], canvasRef)))
    }

    if (numberOfElements !== locations.length)
      setLocations(createElements(numberOfElements, canvasRef))

    let requestId = requestAnimationFrame(update);
    return () => {
      cancelAnimationFrame(requestId);
    };
  });
  return <canvas ref={canvasRef}/>
}


const Game = ({numberOfElements}) => {
  const [xy, setXY] = useState({x: 0, y: 0});

  function onMouseMove(e) {
    setXY({x: e.pageX, y: e.pageY})
  }

  return (
    <div className="game" onMouseMove={onMouseMove}>
      <Canvas
        mouseCoordinates={xy}
        numberOfElements={numberOfElements}
      />
    </div>
  )
}

export default Game;
