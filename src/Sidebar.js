import React, {useState} from "react";


const Sidebar = ({numberOfElements, updateNumberOfElements}) => {
  const [text, setText] = useState(numberOfElements)

  function handleChange(e){
    setText(e.target.value)
  }

  function handleSubmit(e) {
    e.preventDefault();
    updateNumberOfElements(text)
  }

  return (
    <div className="sidebar">
      <form onSubmit={handleSubmit} style={{}}>
        <label>
          Number of Elements:
          <input type="text" pattern="[0-9]+" required="required" title="Please enter a natural number" name="name" value={text} onChange={handleChange}/>
        </label>
        <input type="submit" value="Update" />
      </form>
    </div>
  )
}

export default Sidebar;